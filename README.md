# mpa-bullseye-arm64-rpi

Metalab package archive (MPA) repository for Raspberry Pi OS Bullseye arm64

Fork of https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo

List of packages: https://sat-mtl.gitlab.io/distribution/mpa-bullseye-arm64-rpi/debs/dists/sat-metalab/main/binary-arm64/Packages

## Installation

In a terminal:

```sh
sudo apt install -y coreutils wget && \
wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-bullseye-arm64-rpi/sat-metalab-mpa-keyring.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg && \
echo 'deb [ arch=arm64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-bullseye-arm64-rpi/debs/ sat-metalab main' \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa.list && \
sudo apt update
```

## Automating new packages

- First package your software for Ubuntu: https://packaging.ubuntu.com
- Add user [sat-metalab-mpa](https://gitlab.com/users/sat-metalab-mpa) as Maintainer to the repository of your software (already also added to this MPA), this is required for using the GitLab API with their access token.
- Add the following CI variable to the repository of your software:
  - key: ACCESS_TOKEN
  - value: get from existing repos, for example: https://gitlab.com/sat-mtl/tools/forks/filterpy/-/settings/ci_cd
- In your software repository, `debian/*` branch, append to `.gitlab-ci.yml`:

  ```yaml
  include:
    - remote: 'https://gitlab.com/sat-mtl/distribution/mpa-bullseye-arm64-rpi/-/raw/main/.gitlab-ci-package.yml'
  ```

- In [updaterepos.sh](updaterepos.sh), append the path with namespace of your software repository to variable `projects`, commit, and open a merge request.
